var fs = require('fs');

var obj;

fs.readFile('./data.json', function(err, data) {
  obj = JSON.parse(data);

  obj.hotspot.templates.forEach(function(template) {
    template.id = template.id + 1;
  });

  obj.hotspot.items.forEach(function(item) {
    item.actions.target = item.actions.target + 1;
  });

  obj.card.templates.forEach(function(template) {
    template.id = template.id + 1;
  });

  obj.card.items.forEach(function(item) {
    item.template = item.template + 1;
  });

  console.log(JSON.stringify(obj, null,2));
});
